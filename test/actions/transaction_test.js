import { expect } from '../test_helper';
import { DEPOSIT } from '../../src/actions/types';
import { submitTransaction } from '../../src/actions/transaction_actions';
import * as router from 'react-router'

describe('actions', () => {
    describe('submit transaction', () => {
        it('has the correct type', () => {
            router.browserHistory = { push: () => {} }
            const action = submitTransaction({amount: "10", transactionType: 'deposit'});
            expect(action.type).to.equal(DEPOSIT);
        });

        it('has the correct payload', () => {
            router.browserHistory = { push: () => {} }
            const action = submitTransaction({amount: "10", transactionType:"deposit"});
            expect(action.payload.amount).to.equal("10")
        });
    });
});