import { expect } from '../test_helper';
import TransactionReducer from '../../src/reducers/transaction_reducers';
import { DEPOSIT } from '../../src/actions/types';

describe('TransactionReducer', ()=>{
    it('handles action with unknown type', () =>{
        expect(TransactionReducer(undefined, {})).to.eql({
            userName: 'John',
            balance: 100000,
            withdraw: 0,
            deposit: 0,
            transactions: []
        })
    });

    it('handles action of type DEPOSIT', () => {
        const action = { type: DEPOSIT, payload: {amount: "10", transactionType:"deposit"} }
        const initialData = {
            userName: 'John',
            balance: 100000,
            withdraw: 0,
            deposit: 0,
            transactions: []
        }
        expect(TransactionReducer(initialData, action)).to.eql({
            userName: 'John',
            balance: 100010,
            withdraw: 0,
            deposit: 10,
            transactions: [
                {
                    amount: "10",
                    balance: 100010,
                    type: "deposit"
                }
            ]
        })
    })
});