import { renderComponent, expect } from '../test_helper'
import Transaction from '../../src/components/transaction'

describe('Transaction', () => {
  let component
  beforeEach(() => {
    component = renderComponent(Transaction)
  })
  
  it('component renders', () => {
    expect(component).to.have.class('transaction-container');
  });

});