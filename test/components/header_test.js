import { renderComponent, expect } from '../test_helper'
import Header from '../../src/components/header'

describe('Header', () => {
  let component
  beforeEach(() => {
    component = renderComponent(Header)
  })
  
  it('component renders', () => {
    expect(component).to.have.class('header-menu');
  });

});