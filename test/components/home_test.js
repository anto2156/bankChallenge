import { renderComponent, expect } from '../test_helper'
import Home from '../../src/components/home'

describe('Home', () => {
  let component
  beforeEach(() => {
    component = renderComponent(Home)
  })
  
  it('component renders', () => {
    expect(component).to.have.class('home-container');
  });

  it('displays balance, withdraw, deposit cards', () => {
      expect(component.find('.card').length).to.equal(3)
  });

  it('displays the transaction list', () => {
      expect(component.find('.transaction-list-container'))
  });

});