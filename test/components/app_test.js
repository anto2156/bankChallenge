import { renderComponent, expect } from '../test_helper'
import App from '../../src/components/app'

describe('App', () => {
  let component
  beforeEach(() => {
    component = renderComponent(App)
  })
  
  it('component renders', () => {
    expect(component).to.have.class('app-container');
  });

  it('displays the header', () => {
      expect(component.find('.header-menu'))
  });

});