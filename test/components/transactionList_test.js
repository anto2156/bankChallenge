import { renderComponent, expect } from '../test_helper'
import TransactionList from '../../src/components/transactionList'

describe('TransactionList', () => {
  let component
  beforeEach(() => {
    const props = { 
      data:{
        transactions: [
          {
            amount: '10',
            type: 'deposit',
            balance: '10010'
          },
          {
            amount: '10',
            type: 'withdraw',
            balance: '10000'
          }
        ]
      }
    }
    component = renderComponent(TransactionList,null, props)
  })
  
  it('component renders', () => {
    expect(component).to.have.class('transaction-list-container');
  });

  it('displays transaction type', () => {
    expect(component.find('.header')).to.contain('Transaction Type:')
  })

  it('displays transaction amount', () => {
    expect(component.find('.description')).to.contain('$10')
  })

// .to.contain('Transaction Type: deposit')

});