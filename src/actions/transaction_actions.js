import {
    DEPOSIT,
    WITHDRAW
} from './types';
import { browserHistory } from 'react-router';

// handles users transaction input
export function submitTransaction(transaction){
    // return function(dispatch) returning this as a function is needed when making api requests
    
    // if user makes a deposit
    if(transaction.transactionType === 'deposit'){
        browserHistory.push('/')
        return{ type: DEPOSIT, payload: transaction }
    }
    // if user makes withdrawl
    else if(transaction.transactionType === 'withdraw'){
        browserHistory.push('/')
        return{ type: WITHDRAW, payload: transaction }
    }
}