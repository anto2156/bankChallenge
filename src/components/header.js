import React, { Component } from 'react';
import moment from 'moment';
import { Input, Menu } from 'semantic-ui-react';
import {browserHistory} from 'react-router';

export default class Header extends Component{
    render(){
        // header menu for app navigation
        return(
            <Menu className='header-menu' borderless tabular>
                <Menu.Item>
                    {moment().format('MMMM Do YYYY')}
                </Menu.Item>
                <Menu.Menu position='right'>
                    <Menu.Item name='Make Deposit or Withdrawl' onClick={ ()=>{browserHistory.push('/transaction');} } />
                    <Menu.Item onClick={ ()=>{browserHistory.push('/');} }>
                        <p>Welcome, {this.props.userName}!</p>
                    </Menu.Item>
                </Menu.Menu>
            </Menu>
        )
    }
}
