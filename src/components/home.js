import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Card, Button, Image } from 'semantic-ui-react';
import TransactionList from './transactionList';

class Home extends Component{
    constructor(props){
        super(props)

        this.formatBalance = this.formatBalance.bind(this)
    }

    // formats the int or string into money friendly format
    formatBalance(n){
        return parseInt(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
    }

    render(){
        // render if userData properly mapped to props
        if(this.props.userData){
            return(
                <div className='home-container'>
                    <Grid centered>
                        <Grid.Column>
                            <Card.Group>
                                <Card centered className='balance-card'> 
                                    <Card.Content>
                                        <Card.Header>
                                            <p>Your Balance</p>
                                        </Card.Header>
                                        ${ this.formatBalance(this.props.userData.balance) }
                                    </Card.Content>
                                </Card>
                                <Card centered className='withdraw-card'>
                                    <Card.Content>
                                        <Card.Header>
                                            Total Withdrawn
                                        </Card.Header>
                                        ${ this.props.userData.withdraw === 0 ? ' -' : this.formatBalance(this.props.userData.withdraw) }
                                    </Card.Content>
                                </Card>
                                <Card centered className='deposit-card'>
                                    <Card.Content>
                                        <Card.Header>
                                            Total Deposit
                                        </Card.Header>
                                        ${ this.props.userData.deposit === 0 ? ' -' : this.formatBalance(this.props.userData.deposit) }
                                    </Card.Content>
                                </Card>
                            </Card.Group>
                        </Grid.Column>
                    </Grid>
                    <TransactionList />
                </div>
            )
        }
        // if initial data hasnt mapped to props, render Loading...
        else{
            return (
                <p>Loading...</p>
            )    
        }
    }
}

function mapStateToProps(state){
    return {
        // get inital user data state and map it to props
        userData: state.data
    }
}

export default connect(mapStateToProps, null)(Home)