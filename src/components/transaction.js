import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/transaction_actions';
import { Field, reduxForm } from 'redux-form';
import { Grid, Card, Button, Input } from 'semantic-ui-react';

class Transaction extends Component{
    constructor(props){
        super(props)

        this.renderTextField = this.renderTextField.bind(this)
        this.formSubmit = this.formSubmit.bind(this)
    }

    // pass redux form props onto Semantic UI input
    renderTextField( { input, label, placeholder, meta: { touched, error } } ){
        return (
            <Input
                placeholder= {placeholder}
                error={touched && error}
                {...input}
            />
        )
    }

    formSubmit(data){
        // on form submit, call submitTransaction action and pass the values taken from form
        this.props.submitTransaction(data)
    }

    render(){
        const { handleSubmit } = this.props
        return(
                <Grid className='transaction-container' centered>
                    <form onSubmit={handleSubmit(this.formSubmit)}>
                        <div>
                            <label>Transaction Type</label>
                            <div>
                            <Field name="transactionType" component="select" className="ui dropdown input">
                                <option></option>
                                <option value="deposit">Deposit</option>
                                <option value="withdraw">Withdraw</option>
                            </Field>
                            </div>
                        </div>
                        <div>
                            <label>Amount</label>
                            <div>
                            <Field name="amount" component={this.renderTextField} type="text" placeholder="Amount"/>
                            </div>
                        </div>
                        <div>
                            <Button primary type="submit" >Submit</Button>
                        </div>
                    </form>
                </Grid>
        )
    }
}

// Validation function to prevent submitting form with empty fields
const validate = values => {
    const errors = {}
    if (!values.transactionType) {
        errors.transactionType = 'Required'
    }
    if (!values.amount) {
        errors.amount = 'Required'
    }
    return errors
}

Transaction = reduxForm({
    form: 'transaction',
    validate
})(Transaction)

export default connect(null, actions)(Transaction)