import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './header';
import { Container } from 'semantic-ui-react';

class App extends Component{
    render(){
        // check whether intial user data has loaded before loading <Header/> and children
        if(this.props.userData){
            return (
                <Container className='app-container' fluid>
                    <Header userName={this.props.userData.userName}/>
                    {this.props.children}
                </Container>
            )
        }
        // if initial data hasnt loaded, render Loading...
        else{
            return (
                <p>Loading...</p>
            )
        }
    }
}

function mapStateToProps(state){
    // get inital user data state and map it to props
    return {
        userData: state.data
    }
}

export default connect(mapStateToProps, null)(App);