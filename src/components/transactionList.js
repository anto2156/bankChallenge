import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, List } from 'semantic-ui-react';

class TransactionList extends Component{
    constructor(props){
        super(props)

        this.renderList = this.renderList.bind(this)
        this.formatBalance = this.formatBalance.bind(this)
    }

    // formats the int or string into money friendly format
    formatBalance(n){
        return parseInt(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
    }

    renderList(){
        // check whether any transaction have been made, if so map over transaction and render
        if(this.props.transactionList.length > 0){
            return this.props.transactionList.reverse().map( (item, key) => {
                return(
                    <List.Item key={key}>
                        <List.Content>
                            <Grid centered columns={5}>
                                <Grid.Column>
                                    <List.Header>
                                        Transaction Type:
                                    </List.Header>
                                    <List.Description>
                                        {item.type}
                                    </List.Description>
                                </Grid.Column>
                                <Grid.Column>
                                    <List.Header>
                                        Transaction Amount:
                                    </List.Header>
                                    <List.Description>
                                        ${this.formatBalance(item.amount)}
                                    </List.Description> 
                                </Grid.Column>
                                <Grid.Column>
                                    <List.Header>
                                        Balance:
                                    </List.Header>
                                    <List.Description>
                                        ${this.formatBalance(item.balance)}
                                    </List.Description> 
                                </Grid.Column>
                            </Grid>
                        </List.Content>
                    </List.Item>
                )
            })
        }
        // render if if no transaction have been made yet
        else{
            return(
                <Grid centered columns={5}>
                    <p>There are no transactions</p>
                </Grid>
            )
        }
    }
    render(){
        return(
            <div className='transaction-list-container'>
                <List divided relaxed>
                    {this.renderList()}
                </List>
            </div>
        )
    }
}

function mapStateToProps(state){
    return { transactionList: state.data.transactions }
}

export default connect(mapStateToProps, null)(TransactionList)