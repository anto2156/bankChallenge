import {
    DEPOSIT,
    WITHDRAW
} from '../actions/types';

// initial data to start the application, simulates inital server call response data
let initialData = {
            userName: 'John',
            balance: 100000,
            withdraw: 0,
            deposit: 0,
            transactions: []
        }

export default function(state = initialData, action){
    let newBalance, newDeposit, newWithdraw
    switch(action.type){
        case DEPOSIT:
            // sets new balance equal to current balance + amount deposited
            newBalance = +state.balance + +action.payload.amount
            // sets new deposit amount equal to current deposit amount + amount deposited 
            newDeposit = +state.deposit + +action.payload.amount
            // return new state object
            return { 
                ...state, 
                balance: newBalance,
                deposit: newDeposit,
                transactions:[
                    ...state.transactions,
                    {
                        type: action.payload.transactionType,
                        amount: action.payload.amount,
                        balance: newBalance
                    }
                ]
            }
        case WITHDRAW:
            // sets new balance equal to current balance - amount withdrawn
            newBalance = +state.balance - +action.payload.amount
            // sets new withdrawn amount equal to current deposit amount - amount withdrawn 
            newWithdraw = +state.withdraw + +action.payload.amount
            // return new state object
            return { 
                ...state, 
                balance: newBalance,
                withdraw: newWithdraw,
                transactions:[
                    ...state.transactions,
                    {
                        type: action.payload.transactionType,
                        amount: action.payload.amount,
                        balance: newBalance
                    }
                ]
            }            
    }
    return state
}