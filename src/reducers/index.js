import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form'
import TransactionReducer from './transaction_reducers';

const rootReduxer = combineReducers({
    form,
    data: TransactionReducer
});

export default rootReduxer;